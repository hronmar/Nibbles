#include <algorithm>
#include <chrono>
#include <fstream>
#include <map>
#include <math.h>
#include <ncurses.h>
#include <netdb.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include "game.h"
#include "game_draw.h"
#include "globals.h"
#include "player.h"
#include "sockets.h"

typedef std::chrono::steady_clock Clock;

Game::Game(int players[MAX_PLAYERS], bool netGame/* = false*/)
: m_Level(NULL), m_CurrLevel(1), m_NetGame(false), m_Socket(-1) {
    //init players
    for (int i = 0; i < MAX_PLAYERS; i++) {
        switch (players[i]) {
            case PLAYER_HUMAN:
                m_Players.push_back(new HumanPlayer(i));
                m_LocalPlayers[m_Players.back()->id()] = m_Players.back();
                break;
            case PLAYER_PC_EASY:
                m_Players.push_back(new EasyAIPlayer(i));
                break;
            case PLAYER_PC_MEDIUM:
                m_Players.push_back(new MediumAIPlayer(i));
                break;
            case PLAYER_PC_HARD:
                m_Players.push_back(new HardAIPlayer(i));
                break;
            case PLAYER_NET:
                if (netGame) {
                    m_NetGame = true;
                    m_Players.push_back(new HumanPlayer(i));
                    m_RemotePlayers.push_back(m_Players.back());
                }
                break;
        }
    }
}

Game::~Game() {
    for (auto it = m_Players.begin(); it != m_Players.end(); ++it)
        delete *it;
}

void Game::start() {
    if (m_NetGame && m_Socket == -1) {
        //init network game
        m_Socket = openSrvSocket("::", stoi(SETTINGS["server_port"]));
        if (m_Socket == -1) {
            end("Server initialization failed!");
            return;
        }

        initNetPlayers();
    }

    //free previous level
    freeLevel();

    if (m_Players.empty()) {
        end("There are no players!");
        return;
    }

    //try to load new level
    if (!loadLevel(SETTINGS["levels_path"] + SETTINGS["levels_pref"]
            + std::to_string(m_CurrLevel) + SETTINGS["levels_ext"])) {
        end("Could not load another level!");
        return;
    }

    if (m_NetGame) {
        //send info about level
        for (int cli : m_Clients) {
            uint8_t val = LVL_END;
            send(cli, &val, 1, MSG_NOSIGNAL);
            val = m_H;
            send(cli, &val, 1, MSG_NOSIGNAL);
            val = m_W;
            send(cli, &val, 1, MSG_NOSIGNAL);
        }
    }

    //spawn players
    for (auto it = m_Players.begin(); it != m_Players.end(); ++it)
        if (!(*it)->spawn(m_Level, m_H, m_W, m_Spawns)) {
            freeLevel();
            end("Could not spawn all players!");
            return;
        }

    run();
}

void Game::run() {
    int period = 1000 / stoi(SETTINGS["game_speed"]);

    cbreak();
    nodelay(stdscr, TRUE);
    srand(time(NULL));
    m_BonusChance = 0;
    m_BonusesEaten = 0;

    //main game loop
    while (true) {
        draw();

        if (m_NetGame)
            sendData();

        //give players time to react and detect keys
        auto tStart = Clock::now();
        while (true) {
            int key = getch();
            if (key != ERR) {
                auto keyIter = KEY_MAP.find(key);
                if (keyIter != KEY_MAP.end()) {
                    uint8_t val = keyIter->second;
                    try {
                        m_LocalPlayers.at(val & LVL_PLAYER_MASK)->setDirection(val & LVL_DIRECTION_MASK);
                    } catch (...) {
                    }
                }
            }

            if (m_NetGame) {
                for (size_t i = 0; i < m_RemotePlayers.size(); i++) {
                    uint8_t c;
                    if (recv(m_Clients[i], &c, 1, MSG_DONTWAIT) == 1)
                        m_RemotePlayers[i]->setDirection(c & LVL_DIRECTION_MASK);
                }
            }

            auto tNow = Clock::now();
            auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(tNow - tStart);
            if (ms.count() > period)
                break;
        }

        //move players, those with detected collision will die
        for (auto it = m_Players.begin(); it != m_Players.end(); ++it) {
            bool ateFood;
            if ((*it)->move(m_Level, m_H, m_W, m_Spawns, ateFood)) {
                //player was eliminated
                sendData(true);
                freeLevel();
                end((*it)->name() + " was eliminated!");
                return;
            }
            if (ateFood && ++m_BonusesEaten >= stoi(SETTINGS["score_lvl_limit"])) {
                //load next level
                if (++m_CurrLevel > stoi(SETTINGS["levels_count"])) {
                    sendData(true);
                    freeLevel();
                    end(bestPlayer() + " won!");
                } else
                    start();
                return;
            }
        }

        //spawn bonus with increasing probability
        if (rand() % 100 < m_BonusChance) {
            spawnBonus();
            m_BonusChance = 0;
        } else {
            m_BonusChance++;
        }

    }
}

void Game::spawnBonus() {
    int y, x;

    //try to spawn bonus somewhere
    for (int tries = 0; tries < 5; tries++) {
        y = rand() % m_H;
        x = rand() % m_W;
        if (m_Level[y][x] == LVL_EMPTY) {
            m_Level[y][x] = LVL_FOOD;
            break;
        }
    }
}

void Game::draw() const {
    int maxY, maxX;

    clear();

    getmaxyx(stdscr, maxY, maxX);

    if (m_H + 1 <= maxY && m_W <= maxX) {
        for (int i = 0; i < m_H; i++)
            for (int j = 0; j < m_W; j++)
                drawTile(m_Level[i][j], i, j);

        drawScore(m_Players, m_H, maxY, maxX);
    } else if (maxY >= 5 && maxX >= (int) m_LocalPlayers.size() * 4 && m_LocalPlayers.size() > 0) {
        int widthForPlayer = std::min(m_W, (maxX - 1) / (int) m_LocalPlayers.size() - 1);
        int height = std::min(m_H, maxY - 1);

        drawLine(0, height);

        int col = 1;
        for (auto it = m_LocalPlayers.begin(); it != m_LocalPlayers.end(); ++it) {
            for (int i = 0; i < height; i++)
                for (int j = 0; j < widthForPlayer; j++) {
                    int y = it->second->position().m_Y + i - height / 2;
                    y = (y + m_H) % m_H;
                    int x = it->second->position().m_X + j - widthForPlayer / 2;
                    x = (x + m_W) % m_W;

                    drawTile(m_Level[y][x], i, j + col);
                }

            col += widthForPlayer;
            drawLine(col++, height);
        }

        drawScore(m_Players, height, maxY, maxX);
    } else {
        mvaddstr(0, 0, "resize term please");
    }

    refresh();
}

bool Game::loadLevel(std::string lvlFile) {
    char c;
    std::ifstream f(lvlFile);

    if (!f.is_open())
        return false;

    if (!(f >> m_W >> m_H)
            || m_W < LVL_MIN_W || m_W > LVL_MAX_W
            || m_H < LVL_MIN_H || m_H > LVL_MAX_H)
        return false;

    m_Level = new uint8_t*[m_H];

    for (int i = 0; i < m_H; i++) {
        m_Level[i] = new uint8_t[m_W];
        for (int j = 0; j < m_W; j++) {
            if (!(f >> c)) {
                m_H = i + 1;
                freeLevel();
                return false;
            }
            if (c == SETTINGS["lvl_empty"][0])
                m_Level[i][j] = LVL_EMPTY;
            else if (c == SETTINGS["lvl_wall"][0])
                m_Level[i][j] = LVL_WALL;
            else if (c == SETTINGS["lvl_spawn"][0]) {
                m_Level[i][j] = LVL_EMPTY;
                m_Spawns.push_back(Coord(i, j));
            } else {
                //invalid character
                m_H = i + 1;
                freeLevel();
                return false;
            }
        }
    }

    f.close();

    if ((int) m_Spawns.size() < LVL_MIN_SPAWNS) {
        freeLevel();
        return false;
    }

    return true;
}

void Game::freeLevel() {
    if (m_Level == NULL)
        return;

    for (int i = 0; i < m_H; i++)
        delete [] m_Level[i];

    delete [] m_Level;
    m_Level = NULL;
}

void Game::end(std::string message) {
    int h, w;

    if (m_NetGame && m_Socket != -1) {
        for (int cli : m_Clients)
            close(cli);

        close(m_Socket);
        m_Socket = -1;
    }

    halfdelay(10);
    do {
        getmaxyx(stdscr, h, w);
        clear();

        attron(A_BOLD);
        mvaddstr(1, std::max(0, (w - 12) / 2), "Game ends...");
        attroff(A_BOLD);

        //attron(A_ITALIC);
        mvaddstr(3, std::max(0, (w - (int) message.size()) / 2), message.c_str());
        //attroff(A_ITALIC);

        if (!m_Players.empty())
            drawScore(m_Players, 5, h - 3, w, w / 4);

        mvaddstr(h - 2, std::max(0, (w - 23) / 2), "press enter to continue");

        refresh();
    } while (getch() != '\n');
}

std::string Game::bestPlayer() const {
    int bestScore = -100;
    std::string bestName = "No one";

    for (auto it = m_Players.begin(); it != m_Players.end(); ++it) {
        if ((*it)->score() > bestScore) {
            bestScore = (*it)->score();
            bestName = (*it)->name();
        }
    }

    return bestName;
}

void Game::initNetPlayers() {
    clear();
    //attron(A_ITALIC);
    mvaddstr(0, 0, " Waiting for players...");
    //attroff(A_ITALIC);
    refresh();

    m_Clients.clear();
    for (size_t i = 0; i < m_RemotePlayers.size(); i++) {
        struct sockaddr addr;
        socklen_t addrLen = sizeof (addr);

        m_Clients.push_back(accept(m_Socket, &addr, &addrLen));
    }

    //send info about players to clients
    for (int cli : m_Clients) {
        uint8_t val;
        for (Player* x : m_Players) {
            val = x->id();
            send(cli, &val, 1, MSG_NOSIGNAL);
        }
        val = LVL_END;
        send(cli, &val, 1, MSG_NOSIGNAL);
    }
}

void Game::sendData(bool endGame/* = false*/) const {
    for (size_t i = 0; i < m_Clients.size(); i++) {
        uint8_t val = LVL_START;

        send(m_Clients[i], &val, 1, MSG_NOSIGNAL);

        //send level, row by row
        for (int j = 0; j < m_H; j++)
            send(m_Clients[i], m_Level[j], m_W, MSG_NOSIGNAL);

        //send player score
        for (Player *tmp : m_Players) {
            val = tmp->score();
            send(m_Clients[i], &val, 1, MSG_NOSIGNAL);
        }

        //send player coordinates
        if (endGame)
            val = LVL_END;
        else
            val = m_RemotePlayers[i]->position().m_Y;
        send(m_Clients[i], &val, 1, MSG_NOSIGNAL);
        val = m_RemotePlayers[i]->position().m_X;
        send(m_Clients[i], &val, 1, MSG_NOSIGNAL);
    }
}