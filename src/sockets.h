/*! \file
 *  This file contains functions from file downloaded from 
 *  https://edux.fit.cvut.cz/courses/BI-PA2/_media/net_2016.tgz .
 */
#ifndef SOCKETS_H
#define SOCKETS_H

//! Opens server socket.
/*!
 *  This function is a slightly modified version of a function from file srv.cpp
 *  available at https://edux.fit.cvut.cz/courses/BI-PA2/_media/net_2016.tgz .
 */
int openSrvSocket(const char * srvName, int srvPort);

//! Opens client socket.
/*!
 *  This function is a slightly modified version of a function from file cli.cpp
 *  available at https://edux.fit.cvut.cz/courses/BI-PA2/_media/net_2016.tgz .
 */
int openCliSocket(const char * srvName, int srvPort);

#endif /* SOCKETS_H */