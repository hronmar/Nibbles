/*! \file
 */
#ifndef PLAYER_H
#define PLAYER_H

#include <vector>
#include "globals.h"

//! Y,X coordinates
struct Coord {
    //! Default Coord constructor.
    Coord() : m_Y(0), m_X(0) {
    }

    //! Constructs Coord object with given y,x coordinates.
    Coord(int y, int x) : m_Y(y), m_X(x) {
    }

    //! Apply coordinates.
    /*!
     *  \param[in] x Array which y,x element will be returned
     *  \return Returns reference to element of given array corresponding to this coordinates
     */
    uint8_t &operator()(uint8_t **x) {
        return x[m_Y][m_X];
    }

    friend bool operator==(const Coord & a, const Coord & b) {
        return a.m_Y == b.m_Y && a.m_X == b.m_X;
    }

    friend bool operator!=(const Coord & a, const Coord & b) {
        return !(a == b);
    }

    int m_Y, m_X;
};

//! Returns constant of opposite direction.
uint8_t oppositeDirection(uint8_t dir);

//! Abstract base class for player classes used to represent different players in game.
class Player {
public:

    //! Player class constructor, costructs player object with given ID.
    Player(int id) : m_Id(id), m_WillDie(false) {
        m_Score = stoi(SETTINGS["init_score"]);
    }

    //! Player class destructor.
    virtual ~Player() {
    }

    //! Sets direction the player will move in next call of move(), AI will ignore this.
    virtual void setDirection(uint8_t) = 0;

    //! Move in current direction, respawn if willDie is true.
    /*!
     *  \param[in,out] level The level array in which to move
     *  \param[in] h,w Level dimensions
     *  \param[in] spawns List of coordinates where it is possible to spawn
     *  \param[out] ateFood Indicates whether the player collected food during this move
     *  \return Returns true if the player is eliminated.
     */
    bool move(uint8_t **level, int h, int w, const std::vector<Coord> & spawns, bool & ateFood);

    //! Spawns a player into the level.
    /*!
     *  \param[in,out] level The level array in which to move
     *  \param[in] h,w Level dimensions
     *  \param[in] spawns List of coordinates where it is possible to spawn
     *  \param[in] random Determines if the spawn will be chosen randomly
     *  \return Returns true on success.
     */
    bool spawn(uint8_t **level, int h, int w, std::vector<Coord> spawns, bool random = false);

    //! Returns player name as string.
    std::string name() const {
        return "Player " + std::to_string(m_Id + 1);
    }

    //! Returns player score.
    int score() const {
        return m_Score;
    }

    //! Sets player score to given value.
    void score(int newScore) {
        m_Score = newScore;
    }

    //! Returns player ID.
    int id() const {
        return m_Id;
    }

    //! Returns player's worm head position.
    Coord position() const {
        return m_WormHead;
    }

protected:
    int m_Id;
    uint8_t m_Direction;
    bool m_WillDie;
    int m_Score;
    Coord m_WormHead;
    Coord m_WormTail;

    //! Finds large enough empty space and spawn worm.
    bool spawnWorm(uint8_t **level, int h, int w, Coord where, int length);

    //! Removes player worm from game.
    void removeWorm(uint8_t **level, int h, int w);

    //! Sets direction to correct value.
    virtual void chooseDirection(uint8_t **level, int h, int w);

    //! Checks for collision in current direction.
    bool collisionCheck(uint8_t **level, int h, int w) const;
};

//! Human player class, represents local or remote human player.
class HumanPlayer : public Player {
public:
    //! Human player class constructor.
    HumanPlayer(int id) : Player(id) {
    }

    virtual void setDirection(uint8_t direction) {
        m_Direction = direction & LVL_DIRECTION_MASK;
    }
};

//! Virtual class for computer players.
class AIPlayer : public Player {
public:
    //! AIPlayer class constructor, calls parent constructor.
    AIPlayer(int id) : Player(id) {
    }
};

//! Easy PC player class.
class EasyAIPlayer : public AIPlayer {
public:
    //! Easy PC player class constructor, calls parent constructor.
    EasyAIPlayer(int id) : AIPlayer(id) {
    }

    virtual void setDirection(uint8_t) {
    }

protected:
    virtual void chooseDirection(uint8_t **level, int h, int w);
};

//! Medium PC player class.
class MediumAIPlayer : public AIPlayer {
public:
    //! Medium PC player class constructor, calls parent constructor.
    MediumAIPlayer(int id) : AIPlayer(id) {
    }
    
    virtual void setDirection(uint8_t) {
    }

protected:
    virtual void chooseDirection(uint8_t **level, int h, int w);
};

//! Hard PC player class.
class HardAIPlayer : public AIPlayer {
public:
    //! Hard PC player class constructor, calls parent constructor.
    HardAIPlayer(int id) : AIPlayer(id) {
    }

    virtual void setDirection(uint8_t) {
    }

protected:
    virtual void chooseDirection(uint8_t **level, int h, int w);

    //! Which direction seems best in the current situation.
    int bestDirection(uint8_t **level, int h, int w);
};

#endif /* PLAYER_H */