#include <math.h>
#include <ncurses.h>
#include <sstream>
#include <vector>
#include "globals.h"
#include "game_draw.h"
#include "player.h"

void drawTile(uint8_t tile, int y, int x) {
    if (tile == LVL_EMPTY) {
        safeAttron(COLOR_PAIR(COLOUR_DEFAULT));
        mvaddch(y, x, ' ');
        safeAttroff(COLOR_PAIR(COLOUR_DEFAULT));
    } else if (tile == LVL_WALL) {
        safeAttron(COLOR_PAIR(COLOUR_WALL));
        mvaddch(y, x, 'X');
        safeAttroff(COLOR_PAIR(COLOUR_WALL));
    } else if (tile == LVL_FOOD) {
        safeAttron(COLOR_PAIR(COLOUR_FOOD));
        attron(A_BOLD);
        mvaddch(y, x, '*');
        attroff(A_BOLD);
        safeAttroff(COLOR_PAIR(COLOUR_FOOD));
    } else {
        int playerId = tile & LVL_PLAYER_MASK;
        safeAttron(COLOR_PAIR(COLOUR_PLAYERS + playerId));
        if (tile & LVL_FOOD_EATEN_MASK)
            attron(A_BOLD);
        mvaddch(y, x, SETTINGS["players_chars"][playerId % SETTINGS["players_chars"].length()]);
        if (tile & LVL_FOOD_EATEN_MASK)
            attroff(A_BOLD);
        safeAttroff(COLOR_PAIR(COLOUR_PLAYERS + playerId));
    }
}

void drawLine(int coord, int max) {
    for (int i = 0; i < max; i++)
        mvaddch(i, coord, '|');
}

void drawScore(const std::vector<Player *> & players, int y, int maxy, int maxx, int minx/* = 0*/) {
    int playersPerLine, charsForPlayer;
    std::ostringstream oss;

    if (maxy == y)
        return;
    playersPerLine = ceil((double) players.size() / (maxy - y));

    if (playersPerLine == 0)
        return;
    charsForPlayer = (maxx - minx) / playersPerLine;

    int i = 0;
    for (auto it = players.begin(); it != players.end(); ++it, i++) {
        safeAttron(COLOR_PAIR(COLOUR_PLAYERS + (*it)->id()));

        oss.str("");
        if (charsForPlayer >= 12)
            oss << (*it)->name() << ": " << (*it)->score();
        else
            oss << " " << (*it)->score();

        mvaddstr(y, (i % playersPerLine) * charsForPlayer + minx, oss.str().substr(0, charsForPlayer).c_str());

        if ((i + 1) % playersPerLine == 0)
            y++;

        safeAttroff(COLOR_PAIR(COLOUR_PLAYERS + (*it)->id()));
    }
}


//helper functions

void safeAttron(attr_t att) {
    if (HAS_COLOURS)
        attron(att);
}

void safeAttroff(attr_t att) {
    if (HAS_COLOURS)
        attroff(att);
}