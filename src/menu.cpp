#include <ncurses.h>
#include <string>
#include "game.h"
#include "game_netclient.h"
#include "globals.h"
#include "menu.h"

//constants
const std::string HEADER = "NIBBLES";
const int NUM_CHOICES = 4;
const std::string POSSIBLE_CHOICES[NUM_CHOICES] = {
    "Local game",
    "Host network game",
    "Join network game",
    "Exit"
};
const std::string HINT = "use arrow keys to choose an option and enter to confirm it";
const int PLAYER_TYPES = 6;
const std::string POSSIBLE_PLAYERS[PLAYER_TYPES] = {
    "Human",
    "PC Easy",
    "PC Medium",
    "PC Hard",
    "Remote player",
    "Empty"
};
const std::string HINT_SETUP_1 = "up/down arrow keys to choose player, left/right keys to change type";
const std::string HINT_SETUP_2 = "enter to confirm, backspace to go back";

void Menu::mainMenu() {
    //draws menu in the initial state
    m_Choice = 0;
    drawMenu();

    //redraw the screen every second in case terminal resizes
    halfdelay(10);

    //get user input and respond
    while (true) {
        switch (getch()) {
            case KEY_UP:
                if (--m_Choice < 0)
                    m_Choice = NUM_CHOICES - 1;
                break;
            case KEY_DOWN:
                if (++m_Choice >= NUM_CHOICES)
                    m_Choice = 0;
                break;
            case KEY_RIGHT:
            case '\n':
                if (m_Choice < 2) {
                    //setup game
                    m_GameType = m_Choice;
                    gameSetup();
                } else if (m_Choice == 2) {
                    //join net game
                    NetClient nc;
                    nc.start();
                    halfdelay(10);
                    m_Choice = 0;
                } else {
                    //exit
                    return;
                }
            default:
                break;
        }
        //redraw
        drawMenu();
    }
}

void Menu::gameSetup() {
    bool isNetwork = (m_Choice == 1);

    initSetup();
    drawSetup();

    //get user input and respond
    while (true) {
        switch (getch()) {
            case KEY_UP:
                if (--m_Choice < 0)
                    m_Choice = MAX_PLAYERS - 1;
                break;
            case KEY_DOWN:
                if (++m_Choice >= MAX_PLAYERS)
                    m_Choice = 0;
                break;
            case KEY_LEFT:
                if (--m_Players[m_Choice] < 0)
                    m_Players[m_Choice] = PLAYER_TYPES - 1;
                if (!isNetwork && m_Players[m_Choice] == PLAYER_NET)
                    m_Players[m_Choice]--;
                break;
            case KEY_RIGHT:
                if (++m_Players[m_Choice] >= PLAYER_TYPES)
                    m_Players[m_Choice] = 0;
                if (!isNetwork && m_Players[m_Choice] == PLAYER_NET)
                    m_Players[m_Choice]++;
                break;
            case KEY_BACKSPACE:
                m_Choice = 0;
                return;
            case '\n':
                Game g(m_Players, isNetwork);
                g.start();
                halfdelay(10);
                m_Choice = 0;
                return;
        }

        //redraw
        drawSetup();
    }
}

void Menu::initSetup() {
    m_Choice = 0;
    for (int i = 0; i < MAX_PLAYERS; i++)
        m_Players[i] = 0;
}

void Menu::drawMenu() const {
    clear();

    //print header
    attron(A_BOLD | A_UNDERLINE);
    drawStrCentered(HEADER, 0);
    attroff(A_BOLD | A_UNDERLINE);

    //print options
    for (int i = 0; i < NUM_CHOICES; i++) {
        if (i == m_Choice)
            standout();
        drawStrCentered(POSSIBLE_CHOICES[i], i + 2);
        standend();
    }

    //attron(A_ITALIC);
    //print hint
    drawStrCentered(HINT, NUM_CHOICES + 3, true);
    //attroff(A_ITALIC);

    refresh();
}

void Menu::drawSetup() const {
    clear();

    attron(A_BOLD);
    drawStrCentered(POSSIBLE_CHOICES[m_GameType], 0);
    attroff(A_BOLD);

    //print options
    for (int i = 0; i < MAX_PLAYERS; i++) {
        drawStrCentered("Player" + std::to_string(i + 1) + ":", 3 * i + 2);
        if (i == m_Choice)
            standout();
        drawStrCentered(POSSIBLE_PLAYERS[m_Players[i]], 3 * i + 3);
        standend();
    }

    //attron(A_ITALIC);
    drawStrCentered(HINT_SETUP_1, 3 * MAX_PLAYERS + 3, true);
    drawStrCentered(HINT_SETUP_2, 3 * MAX_PLAYERS + 5, true);
    //attroff(A_ITALIC);

    refresh();
}

void Menu::drawStrCentered(const std::string & str, int y, bool multiline/* = false*/) const {
    int w = getmaxx(stdscr);
    int x = (w - str.size()) / 2;

    if (x < 0) {
        mvaddstr(y, 0, str.substr(0, w).c_str());
        if (multiline)
            drawStrCentered(str.substr(w), y + 1, multiline);
        return;
    }

    mvaddstr(y, x, str.c_str());
}