/*! \file
 */
#ifndef GAME_H
#define GAME_H

#include <map>
#include <string>
#include <vector>
#include "player.h"

//! Main class for running local or network game.
/*!
 *  This class is used to run local or network game, 
 *  in which case it serves as a server.
 */
class Game {
public:
    //! Creates a new game instance.
    /*!
     *  \param[in] players Array of player types constants
     *  \param[in] netGame Whether this will be a network game
     */
    Game(int players[MAX_PLAYERS], bool netGame = false);

    //! Game class destructor.
    ~Game();

    //! Start the game on current level.
    /*!
     *  Loads and initializes current level and starts the game.
     */
    void start();
private:
    uint8_t **m_Level;
    int m_W, m_H;
    int m_CurrLevel;
    std::vector<Player *> m_Players;
    std::vector<Coord> m_Spawns;
    std::map<int, Player *> m_LocalPlayers;
    std::vector<Player *> m_RemotePlayers;
    int m_BonusChance;
    int m_BonusesEaten;
    bool m_NetGame;
    int m_Socket;
    std::vector<int> m_Clients;

    //! Main game function - here is the game loop.
    void run();

    //! Spawns a bonus on random coordinates.
    void spawnBonus();

    //! Displays level.
    void draw() const;

    //! Loads current level.
    /**
     *  @param lvlFile Source file
     *  @return True on success.
     */
    bool loadLevel(std::string lvlFile);

    //! Frees level array resources.
    void freeLevel();

    //! End game and show result.
    /**
     * @param[in] message Text to display in summary
     */
    void end(std::string message);

    //! Returns name of player with the best score.
    /**
     * @return Name of best player
     */
    std::string bestPlayer() const;

    //! Initializes players for network game.
    void initNetPlayers();

    //! Sends data about current game state to clients.
    /**
     * @param[in] endGame If true tell clients the game ends
     */
    void sendData(bool endGame = false) const;
};

#endif /* GAME_H */