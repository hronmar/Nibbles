#include <algorithm>
#include <ncurses.h>
#include <netdb.h>
#include <unistd.h>
#include <vector>
#include "globals.h"
#include "game_draw.h"
#include "game_netclient.h"
#include "sockets.h"
#include "player.h"

void NetClient::start() {
    clear();
    mvaddstr(0, 0, " Joining the game and waiting for other players...");
    refresh();

    m_Socket = openCliSocket(SETTINGS["server_adr"].c_str(), stoi(SETTINGS["server_port"]));
    if (m_Socket == -1) {
        clear();
        mvaddstr(0, 0, "Could not join the game!");
        refresh();
        getch();

        return;
    }

    run();
    freeLvl();
    close(m_Socket);
}

void NetClient::run() {
    bool endedOK = false;

    m_Level = NULL;
    cbreak();
    nodelay(stdscr, TRUE);

    //init
    while (true) {
        uint8_t val;
        if (recv(m_Socket, &val, 1, 0) == 1) {
            if (val == LVL_END)
                break;
            else {
                //we hold these just to track and display score
                m_Players.push_back(new HumanPlayer(val));
            }
        }
    }

    while (true) {
        fd_set rd;
        FD_ZERO(&rd);
        FD_SET(m_Socket, &rd);
        FD_SET(STDIN_FILENO, &rd);

        select(m_Socket + 1, &rd, NULL, NULL, NULL);

        if (FD_ISSET(m_Socket, &rd)) {
            if (!fetchData(endedOK))
                break;

            draw();
        }

        if (FD_ISSET(STDIN_FILENO, &rd)) {
            int key = getch();

            if (key != ERR) {
                auto keyIter = KEY_MAP.find(key);
                if (keyIter != KEY_MAP.end()) {
                    uint8_t val = keyIter->second;
                    send(m_Socket, &val, 1, 0);
                }
            }
        }
    }

    displayEnd(endedOK);

    for (Player *tmp : m_Players)
        delete tmp;
}

bool NetClient::fetchData(bool & ok) {
    uint8_t val, val2;

    if (recv(m_Socket, &val, 1, 0) != 1)
        return false;
    if (val == LVL_END) {
        //info about new level
        //height and width
        if (recv(m_Socket, &val, 1, 0) != 1 || recv(m_Socket, &val2, 1, 0) != 1)
            return false;
        m_H = val;
        m_W = val2;
        resizeLvl();
    } else {
        if (!m_Level || val != LVL_START)
            return false;

        //fetch level data
        for (int i = 0; i < m_H; i++)
            if (recv(m_Socket, m_Level[i], m_W, MSG_WAITALL) != m_W)
                return false;

        //fetch player scores
        for (Player *tmp : m_Players) {
            int8_t score;
            recv(m_Socket, &score, 1, 0);
            tmp->score(score);
        }

        //fetch our coordinates
        if (recv(m_Socket, &val, 1, 0) != 1 || recv(m_Socket, &val2, 1, 0) != 1)
            return false;

        if (val == LVL_END) {
            //game ended ok
            ok = true;
            return false;
        }

        m_Y = val;
        m_X = val2;
    }

    return true;
}

void NetClient::draw() const {
    int maxY, maxX;

    getmaxyx(stdscr, maxY, maxX);
    clear();

    if (m_H + 1 <= maxY && m_W <= maxX) {
        for (int i = 0; i < m_H; i++)
            for (int j = 0; j < m_W; j++)
                drawTile(m_Level[i][j], i, j);

        drawScore(m_Players, m_H, maxY, maxX);
    } else {
        int h = std::min(maxY - 1, m_H);
        int w = std::min(maxX, m_W);
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++) {
                int y = m_Y + i - h / 2;
                y = (y + m_H) % m_H;
                int x = m_X + j - w / 2;
                x = (x + m_W) % m_W;

                drawTile(m_Level[y][x], i, j);
            }

        drawLine(w, maxY);

        drawScore(m_Players, h, maxY, maxX);
    }

    refresh();
}

void NetClient::displayEnd(bool ok) const {
    int h, w;

    halfdelay(10);
    do {
        getmaxyx(stdscr, h, w);
        clear();

        attron(A_BOLD);
        if (ok)
            mvaddstr(1, std::max(0, (w - 13) / 2), "Game ended...");
        else
            mvaddstr(1, std::max(0, (w - 29) / 2), "Connection with server closed!");
        attroff(A_BOLD);

        if (!m_Players.empty())
            drawScore(m_Players, 5, h - 3, w, w / 4);

        mvaddstr(h - 2, std::max(0, (w - 23) / 2), "press enter to continue");

        refresh();
    } while (getch() != '\n');
}

void NetClient::resizeLvl() {
    freeLvl();

    m_Level = new uint8_t*[m_H];

    for (int i = 0; i < m_H; i++) {
        m_Level[i] = new uint8_t[m_W];
        for (int j = 0; j < m_W; j++)
            m_Level[i][j] = LVL_EMPTY;
    }
}

void NetClient::freeLvl() {
    if (m_Level == NULL)
        return;

    for (int i = 0; i < m_H; i++)
        delete [] m_Level[i];

    delete [] m_Level;
    m_Level = NULL;
}