#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include "sockets.h"

//-------------------------------------------------------------------------------------------------
int openSrvSocket(const char * srvName, int srvPort) {
    struct addrinfo * ai;
    char srvPortText[10];

    snprintf(srvPortText, sizeof ( srvPortText), "%d", srvPort);

    if (getaddrinfo(srvName, srvPortText, NULL, &ai) != 0)
        return -1;

    int s = socket(ai -> ai_family, SOCK_STREAM, 0);
    if (s == -1) {
        freeaddrinfo(ai);
        return -1;
    }

    if (bind(s, ai -> ai_addr, ai -> ai_addrlen) != 0) {
        close(s);
        freeaddrinfo(ai);
        return -1;
    }

    if (listen(s, 5) != 0) {
        close(s);
        freeaddrinfo(ai);
        return -1;
    }
    freeaddrinfo(ai);
    return s;
}

//-------------------------------------------------------------------------------------------------
int openCliSocket(const char * srvName, int srvPort) {
    struct addrinfo * ai;
    char srvPortText[10];

    snprintf(srvPortText, sizeof ( srvPortText), "%d", srvPort);

    if (getaddrinfo(srvName, srvPortText, NULL, &ai) != 0)
        return -1;

    int s = socket(ai -> ai_family, SOCK_STREAM, 0);
    if (s == -1) {
        freeaddrinfo(ai);
        return -1;
    }

    if (connect(s, ai -> ai_addr, ai -> ai_addrlen) != 0) {
        close(s);
        freeaddrinfo(ai);
        return -1;
    }
    freeaddrinfo(ai);
    return s;
}