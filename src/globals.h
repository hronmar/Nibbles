/*! \file
 */
#ifndef GLOBALS_H
#define GLOBALS_H

#include <map>

/*  Here are all global constants. Changing them may require 
    additional modifications in code for game to work correctly,
    use config file to customize settings!  */

//! The maximum number of players in one game.
const int MAX_PLAYERS = 4;

//player types constants
const int PLAYER_HUMAN = 0;
const int PLAYER_PC_EASY = 1;
const int PLAYER_PC_MEDIUM = 2;
const int PLAYER_PC_HARD = 3;
const int PLAYER_NET = 4;
const int PLAYER_NONE = 5;

//level limits constants
const int LVL_MIN_W = 10;
const int LVL_MAX_W = 100;
const int LVL_MIN_H = 10;
const int LVL_MAX_H = 100;
const int LVL_MIN_SPAWNS = 4;

//level constants
const uint8_t LVL_EMPTY = 0xff;
const uint8_t LVL_WALL = 0x1f;
const uint8_t LVL_FOOD = 0x3f;
const uint8_t LVL_START = 0x5f;
const uint8_t LVL_END = 0x7f;

//! Mask to filter player ID bits.
const uint8_t LVL_PLAYER_MASK = 0x1f;

//! Number of shifts that must be made to access direction bits.
const int LVL_DIRECTION_START = 6;

//! Mask to filter direction bits.
const uint8_t LVL_DIRECTION_MASK = 0xc0;

//directions: 0=up,1=right,2=down,3=left
const uint8_t LVL_DIRECTION_UP = 0x00;
const uint8_t LVL_DIRECTION_RIGHT = 0x40;
const uint8_t LVL_DIRECTION_DOWN = 0x80;
const uint8_t LVL_DIRECTION_LEFT = 0xc0;

//! Mask to filter bit that indicates if worm ate food on this field.
const uint8_t LVL_FOOD_EATEN_MASK = 0x20;

//! How far AI may look.
const int AI_LIMIT = 50;

//! Default color pair.
const int COLOUR_DEFAULT = 1;
//! Color pair for drawing walls in level.
const int COLOUR_WALL = 2;
//! Color pair for drawing food in level.
const int COLOUR_FOOD = 3;
//! Number of first color pair for player.
const int COLOUR_PLAYERS = 4;

//! This map holds many customizable constants.
extern std::map<std::string, std::string> SETTINGS;

//! Here are control keys for players.
extern std::map<int, uint8_t> KEY_MAP;

//! Indicates whether colours can be used.
extern bool HAS_COLOURS;

#endif /* CONSTANTS_H */