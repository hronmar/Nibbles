/*! \file
 */
#ifndef MENU_H
#define MENU_H

//! Menu class, serves as the main interface.
/*!
 *  This class is used to display and control the main menu and setup a game.
 */
class Menu {
public:
    //! Menu class constructor.
    Menu() : m_Choice(0), m_GameType(0) {
    }

    //! Display main menu and let user choose.
    /*!
     *  Shows the main menu, then lets user select an option and reacts appropriately.
     */
    void mainMenu();

    //! Let the user setup a game.
    /*!
     *  Shows game setup, where man choose who will join the game.
     */
    void gameSetup();
private:
    int m_Choice, m_GameType, m_Players[MAX_PLAYERS];

    //! Make some initializations before game setup.
    void initSetup();

    //! Draws the menu in its current state.
    void drawMenu() const;

    //! Display function for game setup.
    void drawSetup() const;

    //! Prints string in the center of main window on given y-coordinate.
    /**
     *  @param[in] str String to print
     *  @param[in] y Y-coordinate where to print it
     *  @param[in] multiline If true, the string will be wrapped on multiple lines
     */
    void drawStrCentered(const std::string & str, int y, bool multiline = false) const;
};

#endif /* MENU_H */