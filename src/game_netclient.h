/*! \file
 */
#ifndef GAME_NETCLIENT_H
#define GAME_NETCLIENT_H

//! Network game client class.
class NetClient {
public:
    //! Network game client constructor.
    NetClient() {
    }

    //! Starts the client, attempts to connect to server and join game.
    void start();
private:
    int m_Socket;
    int m_H, m_W;
    int m_Y, m_X;
    uint8_t **m_Level;
    std::vector<Player *> m_Players;

    //! Run the client.
    void run();

    //! Draws level received from server.
    void draw() const;

    //! Display end summary.
    /*!
     *  \param[in] ok Indicates if the game ended successively
     */
    void displayEnd(bool ok) const;

    //! Fetches data from server.
    /*!
     *  \param[out] ok Sets this to true if game ended successively
     *  \return Return true if all data were received and game did not end, false otherwise.
     */
    bool fetchData(bool & ok);

    //! Resizes level array.
    void resizeLvl();

    //! Frees level array.
    void freeLvl();
};

#endif /* GAME_NETCLIENT_H */