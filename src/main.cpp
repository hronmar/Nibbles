/*! \file
 */
#include <fstream>
#include <map>
#include <ncurses.h>
#include <string>
#include "globals.h"
#include "menu.h"

using namespace std;

/*!
 *  \mainpage Nibbles
 * 
 *  This is an implementation of Nibbles game (see zadani.txt).
 * 
 *  Use make to compile and generate docs.
 *  One parameter can be specified to program - this is path to configuration file
 *  that can be used to customize the game, see examples for details.
 * 
 */

std::map<std::string, std::string> SETTINGS;
std::map<int, uint8_t> KEY_MAP;
bool HAS_COLOURS;

//! Ncurses initialization.
/*!
 *  Initialize the ncurses library and call all necessary setup functions.
 */
void init() {
    initscr();
    //check for colour support and enable it
    HAS_COLOURS = (has_colors() == TRUE);
    if (HAS_COLOURS) {
        start_color();

        //background
        init_pair(COLOUR_DEFAULT, COLOR_WHITE, COLOR_BLACK);
        bkgd(COLOR_PAIR(COLOUR_DEFAULT));

        //wall and food
        init_pair(COLOUR_WALL, COLOR_WHITE, COLOR_WHITE);
        init_pair(COLOUR_FOOD, COLOR_YELLOW, COLOR_BLACK);

        //player colours
        init_pair(COLOUR_PLAYERS, COLOR_RED, COLOR_BLACK);
        init_pair(COLOUR_PLAYERS + 1, COLOR_GREEN, COLOR_BLACK);
        init_pair(COLOUR_PLAYERS + 2, COLOR_BLUE, COLOR_BLACK);
        init_pair(COLOUR_PLAYERS + 3, COLOR_MAGENTA, COLOR_BLACK);
    }

    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
}

//! Load settings from config file.
/*!
 *  Tries to load settings from user config file, which path can be passed 
 *  as command line argument (default is config.cfg)
 *  \param[in] cfgFile Config file path
 */
void loadSettings(const string & cfgFile) {
    ifstream f(cfgFile);
    string key, value;

    if (!f.is_open())
        return;

    while (f >> key >> value)
        if (key.size() > 15 && key.substr(0, 15) == "controls_player") {
            //load control keys for player
            if (value.size() < 4)
                continue;
            int playerId = stoi(key.substr(15)) - 1;
            for (int i = 0; i < 4; i++) {
                int dir = (4 - i) % 4; //left/right correction
                KEY_MAP[value[i]] = (dir << LVL_DIRECTION_START) | (playerId & LVL_PLAYER_MASK);
            }
        } else if (SETTINGS.find(key) != SETTINGS.end())
            SETTINGS[key] = value;

    f.close();
}

//! Initializes player constrols.
void initControls() {
    //player1
    KEY_MAP[KEY_UP] = LVL_DIRECTION_UP;
    KEY_MAP[KEY_LEFT] = LVL_DIRECTION_LEFT;
    KEY_MAP[KEY_DOWN] = LVL_DIRECTION_DOWN;
    KEY_MAP[KEY_RIGHT] = LVL_DIRECTION_RIGHT;

    //player2
    KEY_MAP['w'] = LVL_DIRECTION_UP | 0x01;
    KEY_MAP['a'] = LVL_DIRECTION_LEFT | 0x01;
    KEY_MAP['s'] = LVL_DIRECTION_DOWN | 0x01;
    KEY_MAP['d'] = LVL_DIRECTION_RIGHT | 0x01;

    //player3
    KEY_MAP['i'] = LVL_DIRECTION_UP | 0x02;
    KEY_MAP['j'] = LVL_DIRECTION_LEFT | 0x02;
    KEY_MAP['k'] = LVL_DIRECTION_DOWN | 0x02;
    KEY_MAP['l'] = LVL_DIRECTION_RIGHT | 0x02;

    //player4
    KEY_MAP['g'] = LVL_DIRECTION_UP | 0x03;
    KEY_MAP['v'] = LVL_DIRECTION_LEFT | 0x03;
    KEY_MAP['b'] = LVL_DIRECTION_DOWN | 0x03;
    KEY_MAP['n'] = LVL_DIRECTION_RIGHT | 0x03;
}

//! Initializes game setting.
/*!
 *  This function sets customizable constants to default values
 *  and loads settings from user supplied file.
 *  \param cfgFile Config file path
 */
void initSettings(const string & cfgFile) {
    //path to level files
    SETTINGS["levels_path"] = "lvl/";
    //prefix and extension of level files
    SETTINGS["levels_pref"] = "lvl_";
    SETTINGS["levels_ext"] = ".txt";
    //game speed
    SETTINGS["game_speed"] = "2";
    //score constants
    SETTINGS["init_score"] = "10";
    SETTINGS["food_gain"] = "1";
    SETTINGS["death_penalty"] = "2";
    SETTINGS["score_lvl_limit"] = "10";
    //possible chars in lvl file
    SETTINGS["lvl_empty"] = "-";
    SETTINGS["lvl_wall"] = "x";
    SETTINGS["lvl_spawn"] = "*";
    //other important game settings
    SETTINGS["worm_init_length"] = "3";
    SETTINGS["players_chars"] = "#$&@";
    SETTINGS["levels_count"] = "3";
    //address of server for network games
    SETTINGS["server_adr"] = "localhost";
    SETTINGS["server_port"] = "2666";

    initControls();

    loadSettings(cfgFile);
}

//! Main function.
int main(int argc, char **argv) {
    if (argc >= 2)
        initSettings(string(argv[1]));
    else
        initSettings("config.cfg");

    init();

    Menu m;
    m.mainMenu();

    endwin();

    return 0;
}