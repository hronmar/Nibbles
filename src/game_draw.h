/*! \file
 */
#ifndef GAME_DRAW_H
#define GAME_DRAW_H

#include <vector>
#include "player.h"

//! Displays one level tile.
/*!
 *  \param[in] tile Value of tile to display
 *  \param[in] y,x Y,X-coordinates where to display on the screen (in terminal)
 */
void drawTile(uint8_t tile, int y, int x);

//! Draws a vertical line.
/*!
 *  Draws a vertical line from (0, coord) to (max, coord) in (y, x) coordinates.
 *  \param[in] coord X-coordinate where to draw the line
 *  \param[in] max Y-coordinate where the line ends
 */
void drawLine(int coord, int max);

//! Displays player score for each player, starting at y.
/*!
 *  \param[in] players Vector of players whose score will be displayed
 *  \param[in] y Y-coordinate where to start drawing the score
 *  \param[in] maxy,maxx Bounds of the area used to display score
 *  \param[in] minx Left offset
 */
void drawScore(const std::vector<Player *> & players, int y, int maxy, int maxx, int minx = 0);

//! Helper function to do attron with has_colours check.
/*!
 *  \param[in] att The attribute to switch on
 */
void safeAttron(attr_t att);

//! Helper function to do attroff with has_colours check.
/*!
 *  \param[in] att The attribute to switch off
 */
void safeAttroff(attr_t att);

#endif /* GAME_DRAW_H */