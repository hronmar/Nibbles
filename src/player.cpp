#include <algorithm>
#include <string>
#include <vector>
#include "globals.h"
#include "player.h"

uint8_t oppositeDirection(uint8_t dir) {
    switch (dir) {
        case LVL_DIRECTION_UP:
            return LVL_DIRECTION_DOWN;
        case LVL_DIRECTION_DOWN:
            return LVL_DIRECTION_UP;
        case LVL_DIRECTION_LEFT:
            return LVL_DIRECTION_RIGHT;
        default:
            return LVL_DIRECTION_LEFT;
    }
}

//up, right, down, left vectors y,x coordinates
const int VECT_Y[] = {-1, 0, 1, 0};
const int VECT_X[] = {0, 1, 0, -1};

void Player::chooseDirection(uint8_t **level, int, int) {
    uint8_t headDir = m_WormHead(level) & LVL_DIRECTION_MASK;
    if (m_Direction == oppositeDirection(headDir))
        m_Direction = headDir;
}

bool Player::collisionCheck(uint8_t **level, int h, int w) const {
    int newY = m_WormHead.m_Y + VECT_Y[m_Direction >> LVL_DIRECTION_START];
    int newX = m_WormHead.m_X + VECT_X[m_Direction >> LVL_DIRECTION_START];

    if (newY < 0 || newY >= h)
        newY = (newY + h) % h;

    if (newX < 0 || newX >= w)
        newX = (newX + w) % w;

    uint8_t newPos = level[newY][newX];

    if (newPos == LVL_EMPTY || newPos == LVL_FOOD)
        return false;

    return true;
}

bool Player::move(uint8_t **level, int h, int w, const std::vector<Coord> & spawns, bool & ateFood) {

    chooseDirection(level, h, w);

    if (collisionCheck(level, h, w)) {
        m_Score -= stoi(SETTINGS["death_penalty"]);
        if (m_Score < 0)
            return true;
        removeWorm(level, h, w);
        spawn(level, h, w, spawns, true);
        return false;
    }

    //remove last
    if ((m_WormTail(level) & LVL_FOOD_EATEN_MASK) == 0x00) {
        uint8_t dir = m_WormTail(level) >> LVL_DIRECTION_START;
        m_WormTail(level) = LVL_EMPTY;
        m_WormTail.m_Y = (m_WormTail.m_Y + VECT_Y[dir] + h) % h;
        m_WormTail.m_X = (m_WormTail.m_X + VECT_X[dir] + w) % w;
    } else {
        m_WormTail(level) ^= LVL_FOOD_EATEN_MASK;
    }

    //set correct direction and move
    m_WormHead(level) &= ~LVL_DIRECTION_MASK;
    m_WormHead(level) |= m_Direction;

    uint8_t dir = m_Direction >> LVL_DIRECTION_START;
    m_WormHead.m_Y = (m_WormHead.m_Y + VECT_Y[dir] + h) % h;
    m_WormHead.m_X = (m_WormHead.m_X + VECT_X[dir] + w) % w;
    if (m_WormHead(level) == LVL_FOOD) {
        m_Score += stoi(SETTINGS["food_gain"]);
        m_WormHead(level) = m_Direction | LVL_FOOD_EATEN_MASK | m_Id;
        ateFood = true;
    } else {
        m_WormHead(level) = m_Direction | m_Id;
        ateFood = false;
    }

    return false;
}

bool Player::spawn(uint8_t **level, int h, int w, std::vector<Coord> spawns, bool random) {
    int len = stoi(SETTINGS["worm_init_length"]);

    if (random)
        std::random_shuffle(spawns.begin(), spawns.end());

    for (auto it = spawns.begin(); it != spawns.end(); ++it)
        if (spawnWorm(level, h, w, *it, len))
            return true;

    return false;
}

bool Player::spawnWorm(uint8_t **level, int h, int w, Coord where, int length) {

    if (where(level) != LVL_EMPTY)
        return false;

    for (int i = 0; i < 4; i++) {
        int y = where.m_Y;
        int x = where.m_X;
        int found = 0;
        do {
            y += VECT_Y[i];
            x += VECT_X[i];
            found++;
        } while (y >= 0 && y < h && x >= 0 && x < w
                && level[y][x] == LVL_EMPTY && found < length);

        if (found == length && level[y][x] == LVL_EMPTY) {
            //found a place for worm

            y = where.m_Y;
            x = where.m_X;

            m_WormTail = Coord(y, x);
            for (int j = 0; j < length; j++)
                level[y + j * VECT_Y[i]][x + j * VECT_X[i]] = (i << LVL_DIRECTION_START) | (m_Id & LVL_PLAYER_MASK);
            m_WormHead = Coord(y + (length - 1) * VECT_Y[i], x + (length - 1) * VECT_X[i]);

            m_Direction = i << LVL_DIRECTION_START;

            return true;
        }
    }

    return false;
}

void Player::removeWorm(uint8_t **level, int h, int w) {

    while (m_WormTail != m_WormHead) {
        uint8_t dir = m_WormTail(level) >> LVL_DIRECTION_START;
        m_WormTail(level) = LVL_EMPTY;
        m_WormTail.m_Y = (m_WormTail.m_Y + VECT_Y[dir] + h) % h;
        m_WormTail.m_X = (m_WormTail.m_X + VECT_X[dir] + w) % w;
    }

    m_WormTail(level) = LVL_EMPTY;
}

//easy ai player

void EasyAIPlayer::chooseDirection(uint8_t **level, int h, int w) {
    if (rand() % 2)
        m_Direction = (rand() % 4) << LVL_DIRECTION_START;

    Player::chooseDirection(level, h, w);
}

//medium ai player

void MediumAIPlayer::chooseDirection(uint8_t **level, int h, int w) {
    int headDir = m_WormHead(level) >> LVL_DIRECTION_START;
    int nextY = (m_WormHead.m_Y + VECT_Y[headDir] + h) % h;
    int nextX = (m_WormHead.m_X + VECT_X[headDir] + w) % w;

    if (level[nextY][nextX] != LVL_EMPTY
            && level[nextY][nextX] != LVL_FOOD) {
        int newDir = rand() % 4;
        //even -> up/down, odd -> left/right
        if (newDir % 2 == headDir % 2) {
            //change it so that we actually change direction
            newDir = (newDir + 1) % 4;
        }
        m_Direction = newDir << LVL_DIRECTION_START;
    } else {
        //we are safe -> look for food
        int limit = std::min(AI_LIMIT, std::min(h, w));
        int dir = (headDir + 1) % 4;

        for (int i = 0; i < 2; i++) {
            int tmpY = m_WormHead.m_Y;
            int tmpX = m_WormHead.m_X;

            for (int j = 1; j <= limit; j++) {
                tmpY = (tmpY + VECT_Y[dir] + h) % h;
                tmpX = (tmpX + VECT_X[dir] + w) % w;

                if (level[tmpY][tmpX] == LVL_FOOD) {
                    m_Direction = dir << LVL_DIRECTION_START;
                    break;
                } else if (level[tmpY][tmpX] != LVL_EMPTY)
                    break;
            }
            dir = (dir + 2) % 4;
        }
    }

    Player::chooseDirection(level, h, w);
}

//hard ai

void HardAIPlayer::chooseDirection(uint8_t **level, int h, int w) {

    m_Direction = bestDirection(level, h, w) << LVL_DIRECTION_START;

    Player::chooseDirection(level, h, w);
}

int HardAIPlayer::bestDirection(uint8_t **level, int h, int w) {
    int headDir = m_WormHead(level) >> LVL_DIRECTION_START;
    int opDir = oppositeDirection(m_WormHead(level) & LVL_DIRECTION_MASK) >> LVL_DIRECTION_START;
    int limit = std::min(AI_LIMIT, std::min(h, w));

    int bestDir = 0;
    int bestScore = -1000;
    int score = 0;
    for (int dir = 0; dir < 4; dir++) {
        if (dir == opDir)
            continue;

        score = 0;
        int tmpY = m_WormHead.m_Y;
        int tmpX = m_WormHead.m_X;
        for (int i = 1; i < limit; i++) {
            tmpY = (tmpY + VECT_Y[dir] + h) % h;
            tmpX = (tmpX + VECT_X[dir] + w) % w;

            if (level[tmpY][tmpX] == LVL_FOOD) {
                score += limit - i;
            } else if (level[tmpY][tmpX] != LVL_EMPTY) {
                if (i == 1)
                    score = -1000;
                break;
            }
        }

        if (dir != headDir) {
            //look for food behind us
            tmpY = m_WormHead.m_Y;
            tmpX = m_WormHead.m_X;
            for (int i = 1; i < limit / 4; i++) {
                tmpY = (tmpY + VECT_Y[opDir] + h) % h;
                tmpX = (tmpX + VECT_X[opDir] + w) % w;
                for (int j = 1; j < limit / 4; j++) {
                    auto tmp = level[(tmpY + VECT_Y[dir] + h) % h][(tmpX + VECT_X[dir] + w) % w];
                    if (tmp == LVL_FOOD)
                        score += limit - i - j - 1;
                    else if (tmp == LVL_WALL)
                        break;
                }
            }
        }

        //prefer not to change direction
        if (dir == headDir)
            score++;

        if (score > bestScore) {
            bestScore = score;
            bestDir = dir;
        }
    }

    return bestDir;
}