PROGRAM=nibbles
CC=g++
LD=g++
FLAGS=-std=c++11 -Wall -Wextra -pedantic -Wno-long-long -O0 -ggdb
LIBS=-lncurses

all: compile doc

run: compile
	./$(PROGRAM)

doc: src/main.cpp src/game_draw.h src/game.h src/game_netclient.h src/globals.h src/menu.h src/player.h src/sockets.h
	doxygen Doxyfile

compile: $(PROGRAM)

$(PROGRAM): game.o game_draw.o game_netclient.o main.o menu.o player.o sockets.o
	$(LD) $(FLAGS) $^ $(LIBS) -o $@

%.o: src/%.cpp
	$(CC) $(FLAGS) -c $< -o $@

game.o: src/game.cpp src/game.h src/player.h src/globals.h \
 src/game_draw.h src/sockets.h
game_draw.o: src/game_draw.cpp src/globals.h src/game_draw.h src/player.h
game_netclient.o: src/game_netclient.cpp src/globals.h src/game_draw.h \
 src/player.h src/game_netclient.h src/sockets.h
main.o: src/main.cpp src/globals.h src/menu.h
menu.o: src/menu.cpp src/game.h src/player.h src/globals.h \
 src/game_netclient.h src/menu.h
player.o: src/player.cpp src/globals.h src/player.h
sockets.o: src/sockets.cpp src/sockets.h

clean:
	rm -rf $(PROGRAM) *.o doc/ 2>/dev/null

